// Bài 1
document.getElementById('sapXep').onclick = function() {
    var soThuNhat = document.getElementById('soThuNhat').value*1;
    var soThuHai = document.getElementById('soThuHai').value*1;
    var soThuBa = document.getElementById('soThuBa').value*1;
    var ketQua = '';
    if (soThuNhat > soThuHai && soThuHai > soThuBa) {
        ketQua = soThuBa + '<' + soThuHai + '<' + soThuNhat;  
    } else if (soThuNhat > soThuHai && soThuHai > soThuBa) {
        ketQua = soThuHai + '<' + soThuBa + '<' + soThuNhat;
    } else if (soThuHai > soThuNhat && soThuNhat > soThuBa) {
        ketQua = soThuBa + '<' + soThuNhat + '<' + soThuHai;
    } else if (soThuHai > soThuBa && soThuBa > soThuNhat) {
        ketQua = soThuNhat + '<' + soThuBa + '<' + soThuHai;
    } else if (soThuBa > soThuNhat && soThuNhat > soThuHai) {
        ketQua = soThuHai + '<' + soThuNhat + '<' + soThuBa;
    } else {
        ketQua = soThuNhat + '<' + soThuHai + '<' + soThuBa;
    }
    document.getElementById('ketQuaBai1').innerHTML = `Kết quả: ` + ketQua;
}
// Bài 2
document.getElementById('loiChaoGiaDinh').onclick = function() {
    var luaChon = document.getElementById('luaChon').value;
    var dad = document.getElementById('bo').value;
    var mom = document.getElementById('me').value;
    var brother = document.getElementById('anhTrai').value;
    var sister = document.getElementById('emGai').value;
    var ketQuaLuaChon = '';
    switch (luaChon) {
        case dad:
            ketQuaLuaChon = 'Xin chào Bố!';
            break;
        case mom:
            ketQuaLuaChon = 'Xin chào Mẹ!';
            break;
        case brother:
            ketQuaLuaChon = 'Xin chào Anh Trai!';
            break;
        case sister:
            ketQuaLuaChon = 'Xin chào Em Gái!' ;
            break;
        default:
            ketQuaLuaChon = 'Xin chào người lạ ơi!';
            break;
        
    }
    document.getElementById('ketQuaBai2').innerHTML = `Kết quả: ` + ketQuaLuaChon;
}
// Bài 3
document.getElementById('dem').onclick = function() {
    var thuNhat = document.getElementById('thuNhat').value*1;
    var thuHai = document.getElementById('thuHai').value*1;
    var thuBa = document.getElementById('thuBa').value*1;
    var demChan = 0;
    if (thuNhat % 2 == 0) {
        demChan ++;
    }
    if (thuHai % 2 == 0) {
        demChan ++;
    }
    if (thuBa % 2 == 0) {
        demChan ++;
    }
    var demLe = 3 - demChan;
    document.getElementById('ketQuaBai3').innerHTML = `Số chẵn là: ` + demChan + `, Số lẻ là: ` + demLe;
}
// Bài 4
document.getElementById('duDoan').onclick = function() {
    var a = document.getElementById('canhThuNhat').value*1;
    var b = document.getElementById('canhThuHai').value*1;
    var c = document.getElementById('canhThuBa').value*1;
    var tamGiac = '';
    if ((a == b && a != c) || (a == c && a != b) || (b == c && b != a )) {
        tamGiac = 'Đây là tam giác cân';
    } else if (a == b && a == c) {
        tamGiac = 'Đây là tam giác đều';
    } else if ((a*a == b*b + c*c) || (b*b == a*a + c*c) || (c*c == a*a + b*b)) {
        tamGiac = 'Đây là tam giác vuông';
    } else {
        tamGiac = 'Đây là tam giác nào đó'
    }
    document.getElementById('ketQuaBai4').innerHTML = `Kết qủa : ` + tamGiac;
}
// Bài 5
document.getElementById('ngayMai').onclick = function() {
    var day = document.getElementById('ngay').value*1;
    var month = document.getElementById('thang').value*1;
    var year = document.getElementById('nam').value*1;
    var day1 = 0;
    var month1 = month;
    var year1 = year;
    switch (month) {
        case 4:
        case 6:
        case 9:
        case 11:
            ngay1Thang = 30;
            break;
        case 2:
            ngay1Thang = 28;
            break;
        default:
            ngay1Thang = 31;
            break;
    }
    day1 =(day%ngay1Thang) + 1;
    if (day == ngay1Thang) {
        month1 = (month%12) + 1;
    }
    if (month%12 ==0 && day == ngay1Thang) {
        year1 ++;
    } 
    document.getElementById('ketQuaBai5').innerHTML = day1 + `/` + month1 + `/` + year1;

}
document.getElementById('ngayHomQua').onclick = function() {
    var day = document.getElementById('ngay').value*1;
    var month = document.getElementById('thang').value*1;
    var year = document.getElementById('nam').value*1;
    var ngay1Thang = 0;
    var day1 = 0;
    var month1 = month;
    var year1 = year;
    
    switch (month - 1) {
        case 4:
        case 6:
        case 9:
        case 11:
            day2 = 30;
            break;
        case 2:
            day2 = 28;
            break;
        default:
            day2 = 31;
            break;
    }
    day1 =(day) - 1;
    if (month == 1 && day == 1) {
        month1 = 12;
        year1 = year - 1;
        day1 = 31;
    } else if (day == 1 ) {
        month1 = month - 1;
        day1 = day2;
    }
    document.getElementById('ketQuaBai5').innerHTML = day1 + `/` + month1 + `/` + year1;
}
// Bài 6
document.getElementById('tinhNgay').onclick = function() {
    var monthBai6 = document.getElementById('nhapThang').value*1;
    var yearBai6 = document.getElementById('nhapNam').value*1;
    var ketQuaTinhNgay = 0;
    switch (monthBai6) {
        case 4:
        case 6:
        case 9:
        case 11:
            ketQuaTinhNgay = 30;
            break;
        case 2: 
            ketQuaTinhNgay = 28 + ((yearBai6%4==0 && yearBai6%100 != 0) || (yearBai6%400 ==0));
            break;
        default:
            ketQuaTinhNgay = 31;
            break;
    }
    document.getElementById('ketQuaBai6').innerHTML = `Tháng ` + monthBai6 + ` năm ` + yearBai6 + ` có ` + ketQuaTinhNgay + ` ngày! `;
}
// Bài 7
document.getElementById('docSo').onclick = function() {
    var n = document.getElementById('soNguyen').value*1;
    var hundreds = Math.floor(n/100);
    var dozens = Math.floor((n%100)/10);
    var units = n%10;
    
    switch (hundreds) {
        case 1:
            doc = 'một trăm'; break;
        case 2:
            doc = 'hai trăm'; break;
        case 3:
            doc = 'ba trăm'; break;
        case 4:
            doc = 'bốn trăm'; break;
        case 5:
            doc = 'năm trăm'; break;
        case 6:
            doc = 'sáu trăm'; break;
        case 7:
            doc = 'bảy trăm'; break;
        case 8:
            doc = 'tám trăm'; break;
        case 9:
            doc = 'chín trăm'; break;
    }
    switch (units) {
        case 1:
            doc1 = 'mốt'; break;
        case 2:
            doc1 = 'hai'; break;
        case 3:
            doc1 = 'ba'; break;
        case 4:
            doc1 = 'bốn'; break;
        case 5:
            doc1 = 'năm'; break;
        case 6:
            doc1 = 'sáu'; break;
        case 7:
            doc1 = 'bảy'; break;
        case 8:
            doc1 = 'tám'; break;
        case 9:
            doc1 = 'chín'; break;
        default:
            doc1 = '';
    }
    switch (dozens) {
        case 0:
            doc2 = 'lẻ'; break;
        case 1:
            doc2 = 'mười'; break;
        case 2:
            doc2 = 'hai mươi'; break;
        case 3:
            doc2 = 'ba mươi'; break;
        case 4:
            doc2 = 'bốn mươi'; break;
        case 5:
            doc2 = 'năm mươi'; break;
        case 6:
            doc2 = 'sáu mươi'; break;
        case 7:
            doc2 = 'bảy mươi'; break;
        case 8:
            doc2 = 'tám mươi'; break;
        case 9:
            doc2 = 'chín mươi'; break;
        default:
            doc2 = '';
    }
    document.getElementById('ketQuaBai7').innerHTML = doc + doc2 + doc1;
}
// Bài 8
document.getElementById('tim').onclick = function() {
    var tenStudent1 = document.getElementById('tenSV1').value;
    var tenStudent2 = document.getElementById('tenSV2').value;
    var tenStudent3 = document.getElementById('tenSV3').value;
    var xStudent1 = document.getElementById('xSinhVien1').value*1;
    var yStudent1 = document.getElementById('ySinhVien1').value*1;
    var xStudent2 = document.getElementById('xSinhVien2').value*1;
    var yStudent2 = document.getElementById('ySinhVien2').value*1;
    var xStudent3 = document.getElementById('xSinhVien3').value*1;
    var yStudent3 = document.getElementById('ySinhVien3').value*1;
    var xSchools = document.getElementById('xTruongHoc').value*1;
    var ySchools = document.getElementById('yTruongHoc').value*1;
    var dSV1_TH = Math.sqrt(Math.pow(xSchools - xStudent1,2) + Math.pow(ySchools - yStudent1,2));
    var dSV2_TH = Math.sqrt(Math.pow(xSchools - xStudent2,2) + Math.pow(ySchools - yStudent2,2));
    var dSV3_TH = Math.sqrt(Math.pow(xSchools - xStudent3,2) + Math.pow(ySchools - yStudent3,2));
    var resultBai8 = '';
    if (dSV1_TH > dSV2_TH && dSV1_TH >dSV3_TH) {
        resultBai8 = tenStudent1;
    } else if (dSV2_TH > dSV1_TH && dSV2_TH > dSV3_TH) {
        resultBai8 = tenStudent2;
    } else {
        resultBai8 = tenStudent3;
    }
    document.getElementById('ketQuaBai8').innerHTML = `Sinh viên xa trường nhất là: ` + resultBai8; 
}
